# pipeline_demo

This repo contains a trivial android app together with a series of commits in two branches to configure a deploy pipeline. 
It was used to demonstrate a CI/CD pipeline setup at an online meetup event hosted by GDG Cape Town in Sept 2020. 

## Event Invite

https://www.meetup.com/Google-Developer-Group-Cape-Town-Meetup/events/272869453/

## Slides
https://docs.google.com/presentation/d/1xftVbGLWAnYCQS9JlfuEQTHubdaryd7EkjYDsn48gL4/edit?usp=sharing


## Recording
https://www.youtube.com/watch?v=-p8TSWvIg1I